const body = document.body;
const divElement = document.createElement("div");
divElement.className = "validate-price";
const inputElement = document.createElement("input");
inputElement.placeholder = "Price";
inputElement.type = "number";
body.prepend(divElement);
divElement.prepend(inputElement);
inputElement.addEventListener("focus", () => {
  inputElement.classList.add("green-border");
  inputElement.style.color = "";
});

inputElement.addEventListener("blur", () => {
  inputElement.classList.remove("green-border");
  isValid();
});

const invalidText = document.createElement("p");

function isValid() {
  if (+inputElement.value < 0) {
    inputElement.classList.add("red-border");
    inputElement.style.color = "";
    invalidText.innerText = "Please enter correct price.";
    inputElement.after(invalidText);
  } else {
    invalidText.remove();
    inputElement.classList.remove("red-border");
    inputElement.style.color = "green";
    const spanElement = document.createElement("span");
    spanElement.className = "current-price";
    spanElement.innerText = `Текущая цена: ${inputElement.value}`;
    divElement.prepend(spanElement);
    const buttonElement = document.createElement("button");
    buttonElement.className = "cancel-button";
    buttonElement.innerText = "x";
    spanElement.append(buttonElement);
    buttonElement.addEventListener("click", () => {
      spanElement.remove();
      inputElement.value = "";
    });
  }
}
